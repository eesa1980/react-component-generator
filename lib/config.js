const COMPONENT_TEMPLATE = 'TypeScriptComponent'

const defaults = {
  tests: false,
  storyBookFile: true
}

const componentFiles = (componentName) => [
  {
    template: `${__dirname}/components/${COMPONENT_TEMPLATE}/Component.tsx`,
    target: `${process.cwd()}/${componentName}/${componentName}.tsx`
  },
  {
    template: `${__dirname}/components/${COMPONENT_TEMPLATE}/index.ts`,
    target: `${process.cwd()}/${componentName}/index.ts`
  },
  {
    template: `${__dirname}/components/${COMPONENT_TEMPLATE}/component.styled.ts`,
    target: `${process.cwd()}/${componentName}/${componentName}.styled.ts`
  },
  {
    template: `${__dirname}/components/${COMPONENT_TEMPLATE}/__test__/Component.test.tsx`,
    target: `${process.cwd()}/${componentName}/__test__/${componentName}.test.tsx`
  },
  {
    template: `${__dirname}/components/${COMPONENT_TEMPLATE}/component.stories.tsx`,
    target: `${process.cwd()}/${componentName}/${componentName}.stories.tsx`
  }
]

module.exports = { defaults, componentFiles }
