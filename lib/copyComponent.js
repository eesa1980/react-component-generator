const fs = require('fs-extra')
const config = require('./config')
const replaceVars = require('./replaceTemplateVars')

const COMPONENT_TEMPLATE = 'TypeScriptComponent'

const args = process.argv.splice(2)
const componentName = args[1]

const copyComponent = async () => {
  if (args[0] === '--name') {
    const componentFiles = config.componentFiles(componentName)
    try {
      await Promise.all(
        componentFiles.map(async (component) => {
          await fs.copy(component.template, component.target)
        })
      )
      await replaceVars(componentFiles, componentName)

      return console.log('success!')
    } catch (err) {
      console.error(err)
    }
  }

  console.log('Please use --name flag to enter component name')
}

module.exports = copyComponent
