## React Component Template

### Instructions

- install gloabally `npm instal -g ./` (run from this project route)
- run command in target dirictory to create new component: `react-component-template --name <component-name>`

#### Versions:

0.0.1 - Ability to create a named component via command

### Changelog

- Generate named files from --name flag
- Added TS template

#### Todo:

- Ability to rename the mudule name variable by component name
- Ability to specify custom tmeplate folder path
- story file name toLowercase
- Add optional flag for test files
- Custom templates with variable inputs
- Add --template flag to point to custom template
